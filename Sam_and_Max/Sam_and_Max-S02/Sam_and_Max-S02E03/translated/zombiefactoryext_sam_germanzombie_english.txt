1) SAM
{Angry} What’s going on here?
2) GERMANZOMBIE
{Questionable}Oh, you’re the Americans, right?
3) GERMANZOMBIE
{Agree}Thanks for the brain, guy!
4) SAM
Hey, Max. We can understand the zombies now.
5) GERMANZOMBIE
{Mean}It was as good as my nana’s homemade Kimmelsplanerschnitzen!
6) MAX
I think “understand” is a bit extreme, Sam.
7) SAM
Jurgen put us in some big machine and then we ended up here!
8) GERMANZOMBIE
Oh ja, guy, you’re totally dead now, okay?
9) SAM
But we’re the Freelance Police! We can’t die!
10) GERMANZOMBIE
I know, hey? None of us can! It’s extremely excellent!
11) 
What happened?
12) SAM
How do we get back to normal? We’re not cut out to be zombies.
13) MAX
Yet another of my life’s ambitions down the crapper.
14) GERMANZOMBIE
Ja, I could tell. You shamble like you’ve still got your souls!
15) 
How do we get back to normal?
16) SAM
What do you mean, we act like we’ve got soul?
17) GERMANZOMBIE
No offense, guy! Lots of zombie kids think it’s cool to act like alive these days!
18) GERMANZOMBIE
We call them “sombies,” but it’s cool, hey!
19) 
We don’t have soul!
20) SAM
How do we get our souls back?
21) GERMANZOMBIE
Don’t know! They say Jurgen does something with them, but I’ve never been in the V.I.P. lounge.
22) SAM
We’ve got to overthrow Jurgen and get our souls back!
23) GERMANZOMBIE
Overthrow the hey what? No way, boy!
24) GERMANZOMBIE
The Zombie Factory is where it’s at, friend!
25) 
We’ve got to defeat Jurgen!
26) SAM
Don’t you want revenge on Jurgen for turning you into a zombie?
27) GERMANZOMBIE
Oh, I was already a zombie, silly guy!
28) GERMANZOMBIE
My company makes experimental new drugs until something goes horribly awry!
29) MAX
Drugs to create a race of super-soldiers?
30) GERMANZOMBIE
Male enhancement!
31) 
Don’t you want revenge?
32) SAM
Well if it’s all the same to you, pal, we’re still taking down Jurgen.
33) GERMANZOMBIE
Oh, okay. Save me the wishbone, hey?
34) 
We’re still going to kill Jurgen.
35) SAM
You enjoyed the brain we gave you?
36) GERMANZOMBIE
Oh ja, it was delicious, he must’ve been really smart!
37) 
Glad you liked the brain.
38) SAM
Sorry we don’t have any more brains to give you.
39) GERMANZOMBIE
No problem, hey! The gang went down to the village to get some more!
40) GERMANZOMBIE
From now on we only eat them from high-up gargoyles, like American style!
41) 
Sorry, no more brains.
42) SAM
So long.
43) GERMANZOMBIE
Catching you later on the flip side, zombie brother!
44) SAM
Farewell.
45) SAM
Auf wiedersehen!
46) SAM
Good night.
