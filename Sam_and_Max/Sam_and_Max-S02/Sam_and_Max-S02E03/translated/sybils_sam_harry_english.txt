1) SAM
Harry Moleman. Last time we saw you, you were... [cut off]
2) HARRY
{Angry}Yeah, yeah, on the moon. {Suspicious} What’s all this I keep {Sad} hearing about zombie attacks?
3) MAX
They’re everywhere!
4) HARRY
You boys have guns, right?
5) MAX
[to Sam] Are we just not getting through to these people?
6) SAM
Yeah, we’re really not that complicated, Harry.
7) SAM
How are things going with you and Sybil?
8) HARRY
Oh, she’s playing hard to get. But women have a hard time resisting my charms.
9) HARRY
[oddly sinister] Oh, she’ll grow to love me. Yes, she will.
10) 
How’s the suitor business?
11) SAM
I hate to break it to you, Harry, but Sybil’s not that into you.
12) MAX
She’s more into the whole hard rock scene.
13) HARRY
Just wait ‘til she gets a load of my oboe.
14) MAX
Was that clumsy innuendo, or a pathetic attempt to sound cool?
15) HARRY
[miserably] {sad} Both.
16) 
Sybil’s not into you.
17) SAM
Do you really think you have a chance with Sybil?
18) HARRY
{happy} I’m in it to win it!
19) 
You have no shot.
20) SAM
{confused}Do you know anything about a “zombie factory?”
21) HARRY
{angry} Zombies don’t MAKE things, they BREAK them!
22) SAM
No, it’s a factory that makes zombies.
23) HARRY
{angry}Inconceivable! That’s like making hate and evil and awfulness!
24) MAX
So you’re saying you don’t like zombies.
25) 
Ever hear of the Zombie Factory?
26) SAM
What have you got against zombies?
27) HARRY
{angry} Disgusting creatures! They’re always breaking things!
28) HARRY
Zombies are why we can’t have nice things.
29) 
What’s wrong with zombies?
30) SAM
Wait, the reason you don’t like zombies is that they break things?
31) HARRY
Yes! Stupid, clumsy undead.
32) SAM
{incredulous} And that bothers you more than the brain-eating?
33) HARRY
{angry} They’re hell on knick-knacks!
34) 
Zombies break things?
35) SAM
Are you still into Prismatology?
36) HARRY
Oh, no. Between the four of us, I wasn’t that into it in the first place.
37) SYBIL
You just relocated to the moon for it.
38) HARRY
Well, think how great “docent” looks on a resume.
39) 
Still into Prismatology?
40) SAM
What are you doing for work these days?
41) HARRY
Well, I’m hoping the stipend from this job holds me over.
42) SAM
Sybil’s paying men to be her suitor?
43) SYBIL
[stage whisper] Of course not! But THEY don’t know that.
44) 
What’re you doing for work?
45) 
So long.
46) SAM
See you later, Harry.
49) 
See you later, Harry.
47) MAX
[“BOO!”] Zombies!
48) HARRY
[frightened] Ahhhhh!
