1) ZOMBIESAM
Nnnngaaahhh!
2) MOVIEFONE
We’re sorry, we can’t understand you. But feel free to keep distributing S.O.L. discs!
3) MOVIEFONE
Take the provided CD launcher and use it to give installation discs to zombies throughout the city.
4) ZOMBIESAM
Nnnngaaahhhh!
5) MOVIEFONE
We’re sorry, we can’t understand you.
90) MOVIEFONE
We’re sorry, we can’t understand you.
6) MOVIEFONE
But if you’d like to share the magic of S.O.L. with your new zombie friends, feel free to keep driving!
7) SINISTAR
Zombies walk funny!
8) SAM
Hiya, COPS. Do you know...
9) MOVIEFONE
Hi there! Are you tired of paying a lot for your internet service?
10) SAM
[confused] What? No, not really.
11) MOVIEFONE
Well, those days are over! Welcome to a new type of internet access: S.O.L.!
12) SYNTH
So simple even a zombie could use it.
13) SINISTAR
Yeah, it’s pretty cool.
14) SAM
Do you know anything about a place called the Zombie Factory?
15) SYNTH
PROCESSING... We have gathered extensive marketing data on zombies.
16) MOVIEFONE
Zombies need an easier, less expensive way to access the internet.
17) MOVIEFONE
That’s why we developed Stuttgart On-Line, for the greatest concentration of newly-active zombies: Stuttgart, Germany!
18) SINISTAR
You’ve unlocked a new area!
19) MAX
What did he say that for?
20) SYNTH
We do not know. He has been like this ever since the accident.
21) SAM
Max, when I’m done yammering, remind me we gotta drive to Stuttgart, quick-like-a-zombie!
22) MAX
[being asked to remind Sam to drive to Stuttgart] Drive to Stuttgart, right, forgot it already.
23) 
Ever hear of the Zombie Factory?
24) SAM
You’re selling the internet to zombies?
25) SYNTH
Not just the internet, but much, much more.
26) MOVIEFONE
From trivia to music, lifestyles to deathstyles, and inventive new recipes for brains, Stuttgart On-Line is the premiere online zombie destination!
27) SAM
Didn’t we destroy the internet?
28) SYNTH
Luckily, Al Gore was able to recreate the internet from his original plans.
29) MAX
What’s on the internet that would interest a droning, glassy-eyed zombie, anyway?
30) SYNTH
Al Gore recreated the internet in his own image.
31) SAM
What’s so great about Stuttgart On-Line?
32) MOVIEFONE
It’s the fun, easy-to-use online experience for zombies and zombies-at-heart.
33) 
What’s so great about S.O.L.?
34) SAM
What happened to Bluster Blaster?
35) SYNTH
There was a... PROCESSING... accident while developing our latest invention.
36) SINISTAR
I’m fine, Sam. Haddock is the best kind of fish! I’m fine, Sam.
37) MOVIEFONE
We regret any inconvenience this may have caused.
38) SINISTAR
I’m fine, Sam. Thanks for asking!
39) SAM
Can you guys fix Bluster Blaster?
40) SINISTAR
They had me fixed before they brought me home.
41) SAM
We just want to pimp our car!
42) SYNTH
We no longer offer car upgrades.
43) MOVIEFONE
Our income has been lower than previously forecast!
44) MAX
[protesting] But this neighborhood has the highest accident rate in five states!
45) SAM
We HAVE spent a lot of this year out of town, Max.
46) MAX
Oh, right.
47) MOVIEFONE
So we’ve devoted our resources to developing the ultimate online experience: S.O.L.!
48) SAM
After all this time, you’re not offering ANY car upgrades?
49) SYNTH
None at all.
50) MOVIEFONE
We regret the inconvenience.
51) SINISTAR
There’s our new invention!
52) SYNTH
It is much too dangerous.
53) MAX
Intrigued! Go on.
54) MOVIEFONE
It is a super high-powered antenna for wireless internet access while on the road!
55) SYNTH
There are problems.
56) MOVIEFONE
Using it could result in catastrophic electrical failure and the spontaneous explosion of your vehicle!
57) MAX
We see. And what are the problems?
58) 
You MUST have an upgrade!
59) SAM
We’ll take that super high-powered antenna!
60) SYNTH
We spent our entire savings on development.
61) MOVIEFONE
We can only give it to you if you’ll help us market S.O.L.!
62) SAM
We like marketing!
63) MAX
And we love shooting things!
64) SINISTAR
I like wearing short pants!
65) 
We want that antenna!
66) SAM
We still want that super-powered antenna.
67) 
We still want that antenna!
68) 
We want to drive!
69) SAM
We want to throw more CDs at zombies!
70) MOVIEFONE
Thank you for helping us make S.O.L. a middling success!
71) SYNTH
We are all out of car attachments.
72) MOVIEFONE
But you can keep driving for more fun promotional decals!
73) SINISTAR
Stickers are fun!
74) SYNTH
Keep promoting S.O.L. to win more fabulous decals.
75) SAM
We want to play your driving game!
76) SYNTH
We have no more prizes available.
77) MAX
We just want to play for the love of driving and hitting things!
78) 
So long.
79) SAM
See you later!
80) SINISTAR
Flying kites is safe and fun!
81) SAM
So long, COPS.
82) SINISTAR
Paste tastes bad!
83) SAM
See you around, COPS.
84) SINISTAR
I like shiny quarters!
85) SYNTH
Would you like to begin driving now?
86) SAM
Affirmative!
87) SYNTH
Initiating augmented reality driving sequence...
88) SAM
Negative.
92) SAM
Hey, COPS!
