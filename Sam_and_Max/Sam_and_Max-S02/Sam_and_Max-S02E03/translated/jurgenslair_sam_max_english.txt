1) ZOMBIESAM
We’re zombies!
2) ZOMBIEMAX
[excited] I know!
3) ZOMBIESAM
How’s undeath treating you, Max?
4) ZOMBIEMAX
It’s surprisingly not all that different from anti-life.
5) ZOMBIESAM
We’ve got to get our souls back!
6) ZOMBIEMAX
I don’t know. I’m enjoying being 21 grams lighter.
7) 
We need our souls!
8) ZOMBIESAM
I never suspected our souls were so obnoxious.
9) ZOMBIEMAX
I’ve been telling you for years we shouldn’t listen to them.
10) 
Our souls are jerks!
11) ZOMBIESAM
Vampires, zombies, and now a monster created from spare body parts.
12) ZOMBIEMAX
I was kind of hoping you’d turn into a werewolf.
13) ZOMBIESAM
Full moon’s not for a few days, Max. There’s still time.
14) 
We’re surrounded by cliches!
15) ZOMBIESAM
That monster sure is a gloomy gus, isn’t he?
16) ZOMBIEMAX
He acts like he’s the ONLY unholy creation who’s ever been hated and feared by everyone he sees.
17) 
That monster is a drag.
18) 
Let’s go.
19) ZOMBIESAM
Ngaaaaahhh!
20) ZOMBIEMAX
Right back atcha, Sam.
21) SAMMAX
[in Max’s body] Be careful with my body, Max.
22) MAXSAM
[in Sam’s body] Don’t be such a baby. You’re a lot more damage-resistant than I ever imagined!
23) 
Be careful with my body.
24) SAMMAX
[in Max’s body] How can you tolerate being this short?
25) MAXSAM
[in Sam’s body] I over-compensate with my personality and charm. And lots of bench presses.
26) 
I’m too short!
27) SAMMAX
[in Max’s body] How’s the weather up there? You should play basketball!
28) MAXSAM
[in Sam’s body] We’ll see who’s laughing once I put incriminating photos of your body on the internet.
29) 
You’re too tall!
30) SAMMAX
[in Max’s body] We’ve got Jurgen on the run now!
31) MAXSAM
[in Sam’s body] How do you figure? He’s stronger, faster, and already killed us once today!
32) SAMMAX
[in Max’s body] My pep talks are a lot more convincing coming from my body.
33) 
We’ve got him on the run!
34) SAMMAX
[in Max’s body] Go bite Jurgen or something.
35) MAXSAM
[in Sam’s body] I’m still just trying to figure out how to walk with your weird legs.
36) 
Sic ‘em, Max!
37) SAMMAX
[in Max’s body] What’s the best way to kill a vampire, Sam? I mean Max?
38) MAXSAM
[in Sam’s body] Ruin every pleasure he gets out of life until he wants to kill himself.
39) SAMMAX
[in Max’s body] Wow, that’s kind of dark.
40) MAXSAM
[in Sam’s body] Oh wait... did you mean “best” like “quickest,” or “most satisfying?”
41) 
How do you kill a vampire?
42) SAMMAX
[in Max’s body] At least we’re not zombies anymore.
43) MAXSAM
[in Sam’s body] I was hoping that being a decaying corpse would help you shed a few pounds, Sam.
44) SAMMAX
[in Max’s body] That’s not nice. You’re going to make one of us self-conscious.
45) MAXSAM
[in Sam’s body] Which one?
46) SAMMAX
[in Max’s body] I haven’t figured out yet.
47) 
At least we’re not zombies.
48) SAMMAX
[in Max’s body] Hey, Sam, I mean Max.
51) 
Hey, Sam, I mean Max.
49) MAXSAM
[in Sam’s body] Wait, were you talking to me?
50) SAMMAX
[in Max’s body] I’m not sure.
52) MAXSAM
[in Sam’s body] I’m so confused!
53) SAMMAX
[in Max’s body] Let’s get back to vampire slaying!
54) MAXSAM
[in Sam’s body] *I* never stopped!
55) SAMMONSTER
[in monster’s body] It’s crowded in here!
57) 
It’s crowded in here.
56) MAXMONSTER
[in monster’s body] Quit hogging the brain!
58) SAMMONSTER
[in monster’s body] It’s hard to think with another voice in my head.
59) MAXMONSTER
[in monster’s body] Ehh, after the first dozen you stop noticing.
60) 
It’s hard to concentrate.
61) SAMMONSTER
[in monster’s body] Are you going to help me defeat Jurgen or what?
62) MAXMONSTER
[in monster’s body] You’re doing fine, Sam. I’m still trying to get this guy’s bladder to work.
63) 
You’re not helping!
64) SAMMONSTER
[in monster’s body] Stay close, little buddy.
65) MAXMONSTER
[in monster’s body] Will do!
66) 
Stay close!
