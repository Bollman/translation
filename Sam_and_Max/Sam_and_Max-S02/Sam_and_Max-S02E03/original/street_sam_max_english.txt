1) 
Where’s the Zombie Factory?
2) SAM
We’ve got to find out where this “zombie factory” is.
3) MAX
And if they use robots to make the zombies, because that would be the coolest thing that ever existed!
4) SAM
This has to be our creepiest, most ghoulish case yet, Max!
5) MAX
Nah, that’s still the guy who wanted us to find his missing back hair.
6) SAM
Oooh, right. I’d managed to block that one from my memory until now.
7) 
This is our creepiest case yet!
8) SAM
Aren’t you proud, Max? Our line of work has us exploring the mysteries of life and death for the betterment of all mankind!
9) MAX
I like shooting stuff.
10) 
I love my job!
11) SAM
I wonder why the zombies haven’t been attacking us.
12) MAX
I wonder why you keep setting yourself up for jokes about having no brains.
13) 
The zombies aren’t attacking us.
14) SAM
At least the zombies are mostly behaving themselves.
15) MAX
Our so-called civilization, with its wars and bureaucracy, and thousands of everyday injustices -- aren’t we the real monsters?
16) SAM
That would almost be profound if anybody other than you had said it.
17) 
The zombies are well-behaved.
18) 
Let’s go.
38) 
Let’s go.
19) SAM
Keep your eyes peeled, Max.
20) MAX
They’re scrubbed, peeled, and ready to burst, Sam!
21) SAM
Stay alert, Max!
22) SAM
We’re zombies!
23) MAX
[excited] {happy} I know!
24) SAM
I hope losing Jesse James’ hand taught that zombie a lesson about stealing.
25) MAX
Or at least about casing the joint first, to make sure the owners aren’t home.
26) 
Jesse’s hand took off!
27) SAM
Have you tried the COPS’ online service for zombies yet?
28) MAX
Yeah, I keep getting spammed with job offers from mad scientists.
29) 
Have you tried S.O.L.?
30) SAM
The neighborhood doesn’t seem to be affected by our sudden unexpected death.
31) MAX
I expected some celebratory banners at LEAST.
32) 
Nobody cares we’re dead!
33) SAM
This dying business has reminded me -- I need to sign my organ donor card.
34) MAX
Ooh, leave me your corneas, Sam!
35) SAM
But you have perfect vision.
36) MAX
What’s your point?
37) 
I need to sign my organ donor card.
39) SAM
Keep on shamblin’.
