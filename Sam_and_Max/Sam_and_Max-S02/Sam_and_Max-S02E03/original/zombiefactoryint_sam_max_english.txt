1) SAM
What would you do if you were a zombie, Max?
2) MAX
What WOULDN’T I do? I’ve got two years’ worth of stuff planned to do to my junior high school teachers alone.
3) 
Ever wish you were a zombie?
4) SAM
I never expected we’d end up in a creatures-of-the-nightclub, did you, Max?
5) MAX
It was always my secret wish I dared not speak aloud!
6) 
I didn’t expect a nightclub.
7) SAM
This place isn’t as cool as we’d been led to believe.
8) MAX
Seriously. Pacifiers and glow sticks? Just because THEY’RE dead doesn’t mean STYLE has to be.
9) 
This place is dead.
10) SAM
I always pictured a rave as being more crowded.
11) MAX
There are probably strict fire codes, seeing as how zombies and vampires are so gloriously flammable.
12) 
Not much of a crowd.
13) SAM
I don’t think that Jurgen guy is all that cool.
14) MAX
These zombies have embarrassingly low standards.
15) 
Jurgen’s a loser.
16) SAM
Do you find that Jurgen guy to be as fey and off-putting as I do?
17) MAX
He’s single-handedly sent “vampires” way way down on my list of “unholy creatures I’d like to meet and/or become.”
18) 
Jurgen disgusts me.
19) SAM
What’s something that vampires hate, Max?
20) MAX
Coffee-flavored ice cream.
21) SAM
Well, naturally. But I was hoping for something specific to vampires.
22) 
What do vampires hate?
23) 
So long.
24) SAM
Stay cool, Max.
25) MAX
Always.
26) 
And then what happened?
27) SAM
And then we wandered around the Zombie Factory, looking for things to do.
28) MAX
We’re not in flashback anymore, Sam.
29) SAM
Oh, right.
30) SAM
I wish we had our souls back.
31) MAX
It’s funny, Sam. I like just about everything about being a zombie, except for the constant itchiness.
32) SAM
You noticed that too?
33) SAM
Go figure. This place isn’t any cooler as zombies.
34) MAX
Maybe we have to die again and become European to really appreciate it.
35) 
This place is still uncool.
36) SAM
Feel like dancing, little buddy?
37) MAX
Not until 3 AM, when they break out the foam and the red-striped hats.
38) 
Feel like dancing?
39) SAM
I wonder what Flint Paper’s doing here.
40) MAX
The same thing he does everywhere: kicking ass.
41) 
Why’s Flint Paper here?
42) SAM
I miss Zombie Abraham Lincoln already.
43) MAX
Me too, but it was worth it to see that savage beat-down Flint gave him.
44) SAM
Yeah, it’s a bittersweet loss. That was epic!
45) 
I miss Zombie Lincoln.
46) 
Later!
47) SAM
Nggaaahh.
48) MAX
You too!
