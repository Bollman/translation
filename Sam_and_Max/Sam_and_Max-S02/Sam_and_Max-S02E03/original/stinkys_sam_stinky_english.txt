1) SAM
{confused}Do you know anything about a “zombie factory?”
2) STINKY
{happy}Oh sure. I was a foreman at a zombie factory for a year while I was writing my opera.
3) STINKY
{normal}They’re good workers,{pretentious} as long as you don’t let them unionize.
4) SAM
No, this isn’t a factory that HIRES zombies. It’s a factory that MAKES zombies.
5) STINKY
[caught in a lie]{normal} Oh. Well. Then... did you say “zombies?” I thought you said “elves.”
6) 
Ever hear of the Zombie Factory?
7) SAM
{confused}Zombies giving you much trouble, Stinky?
8) MAX
{suspicious}If that IS your real name.
9) STINKY
Oh, no. They’re a {pretentious}nuisance, but at least THEY eat quickly and leave.
10) STINKY
{normal}Your friend Lincoln is the real horror.
11) 
Zombies got you down?
12) SAM
{worried}C’mon, give Abe a break. He’s on the rebound.
13) STINKY
He keeps trying to pay me in Confederate money!{pretentious} Who DOES that?
14) STINKY
That doesn’t even make SENSE!
15) 
Give Abe a break!
16) SAM
{suspicious}[accusatory] So, have you heard from Grandpa Stinky lately?
17) MAX
{suspicious}Like, from {devilish}BEYOND THE GRAVE?
18) STINKY
What are you talking about? Grandpa’s still on his Himalayan mountain-climbing junket.
19) 
Have you heard from Grandpa?
20) SAM
{suspicious}I thought you said Grandpa Stinky was on an around-the-world cruise!
21) MAX
{devilish}[sneering] Let’s watch as the spider tries to free herself from her own WEB of LIES!
22) STINKY
{pretentious}Oh, he sent me a postcard. His cruise was hijacked by Sherpa terrorists.
23) STINKY
They ended up making him their god, and now they’re following him on a quest to scale the world’s tallest peaks.
24) MAX
{incredulous}Well, her story DOES seem to hold up.
25) 
I thought Stinky was on a cruise!
26) SAM
{sarcastic}It was pretty nice of Grandpa Stinky to leave his entire restaurant empire to you.
27) STINKY
Some empire. Zombies, deadbeat ex-presidents, trivia cheaters... {pretentious}makes me wonder if it was worth the effort.
28) MAX
{sarcastic}If WHAT was worth the effort?
29) STINKY
{normal}Oh, nothing.
30) 
Nice of Stinky to leave you all this.
31) SAM
{confused}What’s with the sunlamp, {stern}woman who we’ll call Stinky for the sake of convenience?
32) STINKY
So THAT’S what that is. I mean...{happy} of COURSE that’s what that is.
33) STINKY
{pretentious}[defensive] Have to look my best, you know!
34) MAX
The magazines set such an unrealistic standard of beauty for grandpa-murderers.
35) 
What’s with the sunlamp?
36) SAM
Can we have your sunlamp?
37) STINKY
{pretentious}What could YOU possibly need it for?
38) SAM
Max needs a UV bulb for his teeth-bleaching regimen.
39) MAX
You think a smile this white comes naturally?
40) STINKY
{normal}Well, go ahead. I don’t need{pretentious}... I mean, I’m done with it.
41) SAM
{confused}What’s a sunlamp doing in a restaurant?
42) MAX
{confused}Did you think the food wasn’t hazardous enough without the threat of melanoma?
43) STINKY
{happy}Oh, that. I was just going through some of the old things Stinky... {normal}Grandpa had in storage.
44) STINKY
{normal}I need a big box for packing... things... that need to be sen{pretentious}t... away.
45) 
Why do you have a sunlamp?
46) 
We want to order something.
47) SAM
We’d like to order something.
48) STINKY
Supplies are low because of the zombie attacks, so, uh, we’re probably out.
49) MAX
You do realize that’s not going to stop us, right?
50) STINKY
[resigned] Go ahead.
51) SAM
We’d like to waste time by ordering stuff you don’t have.
52) STINKY
Knock yourselves out.
53) SAM
Bye.
54) SAM
The special.
55) STINKY
Ah, my special gooey molasses tar cake!{pretentious} It’s an ancient family recipe!
56) MAX
{confused}Ancient?
57) STINKY
You’ve heard of the La Brea Tar Pits, of course. But have you ever TASTED them?
58) SAM
{happy}Sold! I’ll take two slices.
59) STINKY
{normal}Sorry, I used up the blowtorch on some zombies who didn’t tip. It’s impossible to cut.
60) STINKY
You’re welcome to look at it, though. Just don’t touch!
61) STINKY
Sorry, it’s impossible to cut it.
62) SAM
Crab and salamander enchiladas in green sauce.
63) STINKY
Sal! I want Lou Dobbs and Greta Van Susteren doing double-dutch on the back of a rogue elephant!
64) 
Enchiladas.
65) SAM
Tuna noodle casserole with onion rings.
66) STINKY
Get me one Rudy Giuliani with no pants and sequined garters!
67) STINKY
And a tuna noodle casserole with onion rings!
68) 
Tuna noodle casserole.
69) SAM
Seared grouper in a maple syrup reduction, with bacon-wrapped dates and toffee chips, served on a bed of Venezuelan newspaper clippings.
70) STINKY
Sal! Number 3!
71) 
I’d like the fish.
72) SAM
Chicken Chow Mein with chocolate covered raisins and a caramel swirl.
73) STINKY
Sal, get me a hyperactive spider monkey in a powder-blue cardigan. And why don’t we go ahead and wrestle him to the ground and tickle him ‘til he pees!
74) MAX
I have GOT to see what goes on in this kitchen!
75) 
Chicken chow mein.
76) 
Nothing.
77) SAM
Nothing for us, thanks.
