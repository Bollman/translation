1) XMAS_PAST
Teď jsme ve vaší kanceláři v nedávné minulosti.
2) SAM
Vzpomíním si! Tenkrát jsme museli Hračkářské mafii vrátit masový sendvič, který se Leonard snažil střelit Jimmymu!
3) MAX
Takže máme Leonarda zmlátit ještě víc?
4) XMAS_PAST
V žádném případě, Maxi. Tihle lidé nedokáží nijak pocítit tvoji přítomnost, chápeš?
5) XMAS_PAST
Jste tady abyste napravili velkou špatnost, kterou jste spáchali!
6) SAM
Má sami sebe donutit přestat s těmi vtipy o mámě?
7) XMAS_PAST
Tak sleduj!
8) PASTMAX
Nashle pozdějc, trapáku.
9) LEONARD
Počkat, copak vy me nerozvážete?
10) LEONARD
Haló?
11) LEONARD
Borci?
12) LEONARD
Jimmy?
13) LEONARD
Kdokoliv?
14) MARY
[z díry] Tak, Jimmy? Kde je ta úžasná událost, která to všechno zase změní?
15) MARY
Kde jsou všechny ty peníze, které mají zachránit našeho malého Timmyho?
16) TIMMY
Jo, tati. Kde *CENZURA* jsou?
17) MARY
Oh, je to beznadějné! Doktoři říkali, že to ten Tourettův syndrom zabije pokud nepůjde rychle na operaci!
18) TIMMY
Nebreč, mami! Dostanu se do *CENZŮRA* za *CENZŮRA* *CENZŮRA* anděly!
19) JIMMY
Všechno je to na nic, Mary, pokaždé když se zkusím začít živit poctivě, Sam a Max mi to zkazí!
20) TIMMY
Ah, *CENZŮRA*!
21) MARY
Jimmy, všichni na tebe spoléháme!
22) JIMMY
Já vím, Mary, Já vím!
23) JIMMY
Kdybych byl býval zůstal u boxu! Mohl jsem BÝT někdo! 
24) JIMMY
Pořádná krysa, to je to co jsem.
25) MARY
Tak si vem svou šťastnou rukavici a běž boxovat!
26) JIMMY
Ale... Ale já už ji nemám! Sam a Max mi ji vzali!
27) MARY
Zase začínáš se Samem a Maxem! Kdybys míň čuměl na televizi a víc času věnoval boxování, tak jsi mohl být šampion!
28) MARY
Přiznej si to, Jimmy, jsi tragéd!
29) TIMMY
Mami a tati, nehádejte se prosím! Táta je můj hrdina a zase dá všechno do pořádku!
30) TIMMY
Uvidíš! To *CENZŮRA* uvidíš! To *CENZŮRA* *CENZŮRA* *CENZŮRA* *CENZŮRA*!!!! [poslední pípnutí je velmi dlouhé]
31) MARY
Můj Bože! Zavolám doktora, aby ukončil jeho trápení!
32) MARY
Prostě si vezmi tu rukavici, Jimmy! Naše dítě už nemá moc času!
33) XMAS_PAST
A teď, Same a Maxi, víte, co musíte udělat.
34) MAX
Zmlátit Leonarda ještě víc?
35) XMAS_PAST
Povzdech.
36) SAM
Raději bys na to neměl sahat, jinak vytvořím alternativní budoucnost, kde věšáky vládnou světu.
37) SAM
Mravenčí farmy z minulosti jsou tak primitivní.
38) SAM
Některé věci nikdy nevyjdou z módy.
39) MAX
A některé věci do ni nikdy nevejdou. [stylově]
40) SAM
Proč lidé stále používají nástěnky v době, kdy máme počítače?
41) MAX
Zápíchávání těch připínáčků by ty monitory zničilo.
42) SAM
Řekl bych, že ignorování kalendáře je v pohodě.
43) MAX
My máme kalendář?
44) SAM
Měli bychom do Maxova pracovního stolu nacpat víc přerostlých dětí.
45) SAM
Cestovali jsme tak daleko do minulosti... a stejně je už příliš pozdě zachránit koblihy!
46) SAM
Opravdu jsem si myslel, že už se na to nikdy nebudu muset dívat!
47) SAM
Alespoň máme spoustu času dodělat papírování.
48) SAM
Na tenhle obrázek jsem skoro zapomněl.
49) MAX
Měli bychom si to vyfotit, abychom na to nikdy nezapomněli.
50) SAM
To že na Vánoce stojíme pod rukou Jesseho Jamese znamená, že si musíme dát pusu?
51) MAX
To je jmelí.
52) SAM
Oh.  [moje chyba]
86) MARY
Oh.
53) SAM
Víš, na Ebay bychom za to mohli dostat nějaké peníze...
54) MAX
Zapoměň na to. Dostali jsme zaplaceno dolary z roku 2006 a ty jsou v zásadě bezcenné.
55) SAM
Vzchop se, Jimmy. Vrátíme ti tu boxerskou rukavici. A jo vlastně, on nás neslyší.
56) SAM
Tu lampu si pamatuji!
57) MAX
A já si pamatuji, že jsi mi říkal, že její vnitřnosti budou chutnat jako borůvky se smetanou, ty Lháži!
58) SAM
Hej, Leonarde.
59) MAX
Povstali jsme z hrobu, abychom tě přišli strašit!
60) SAM
On nás neslyší, Maxi. A také... nejsme mrtví.
61) MAX
Zatím.
62) SAM
On nás neslyší.
63) SAM
Měl bych zavolat a objednat nám pizzu na večer.
64) MAX
[hlubokým hlasem] "Zdravím, chtěl bych doručit pizzu ode dneška za šest měsíců..."
65) SAM
Telefonní stolky už nejsou to, co bývaly. Počkat, vlastně jsou.
66) SAM
Páni. Hubert vypadá... tak mladě.
67) SAM
Takže Jimmy tam celou tu dobu měl ještě ženu a dítě.
68) SAM
Doufám, že paní Dvojzubková ocení prvotřídní skřítčí řemeslo.
69) SAM
Co teda máme dělat?
70) XMAS_PAST
Copak nedokážeš pobrat ty signály, co dostáváš, chlape?
71) XMAS_PAST
Churavé dítě, otravná stará žena, boxer co ztratil sebedůvěru?
72) MAX
Aha! Pasti na krysy.
73) XMAS_PAST
Ne! Musíte ho dostat zpátky do ringu, člověče!
74) SAM
Cože jsi to chtěl, abychom tady udělali?
75) XMAS_PAST
Ta krysa se musí dostat zpátky do své boxerské formy, člověče!
76) SAM
Jestli mě to naučilo jednu jedinou věc, tak to že dobré ploty NEDĚLAJÍ dobré sousedy!
77) MARY
Jimmy, jestli tu televizi nevypneš, tak ti... Hele, kde to jsem?
78) MARY
Je to beznadějné! Není tu žádné východisko!
79) JIMMY
To něříkej, Mary, můžeme to vyřešit! Já...
80) JIMMY
Mary?! Ona mě opustila! Teď už ji nikdy neuvidím!
81) TIMMY
Ta *CENZŮRA*! [dlouhé pípnutí]
82) JIMMY
Já vím, synu, já vím.
83) MARY
Jamesi Tiberie Dvojzubko! Tu televizi hned teď vypneš!
84) JIMMY
Já nic neudělal! Ta televize se pořád zapíná a vypíná sama od sebe.
85) MARY
Čekáš, že ti snad tohle budu věřit, ty...
87) MARY
Jimmy, vypni tu televizi a vrať se zpátky libovat si ve své bídě!
88) JIMMY
Nech mě být, ty babizno!
89) MARY
Jimmy, vypni tu televizi!
90) SAM
Neboj se malý Jimmy, nedopustíme, aby tě Tourette dostal. Počkat, on nás neslyší.
91) SAM
Náš kazety požírající přehrávač za rozumnou cenu.
92) SAM
Ulice vypadá podstatně méně zničeně.
93) SAM
Tohle by paní Dvojzubkové ani nám nijak nepomohlo.
94) SAM
Šťastné svátky, Jimmy.
95) JIMMY
[naštvaně]{Naštvaně} Hej! Kdo to hodil?
96) JIMMY
{Normálně} Co? Může to být vůbec pravda...? {Štastně} Timmy, koukej? To je tátova šťastná boxerská rukavice!
97) TIMMY
{Normálně} Odkud to *CENZURA* přišlo, tati?
98) JIMMY
Nemám tušení, synku. Ale vím, že jsem koněčně dostal druhou šanci!
99) TIMMY
{Šťastně} To je *CENZURA* *CENZURA* Vánoční zázrak!
100) JIMMY
Odteďka už si tohle budu hlídat jako oko v hlavě!
101) JIMMY
{Normálně} Teď uvidíš, synku! Tvůj táta pořádně nakope pár zadků a vydělá ti na tu operaci!
102) TIMMY
{Normálně} Bůh nás *CENZURA* všechny!
103) XMAS_PAST
Čas jít, borci! Zachránili jste minulé Vánoce!
104) XMAS_PAST
Tak vyrazíme zpátky do přítomnosti!
105) MAX
Ok, kamaráde!
106) SAM
Myslím, že by Jimmymu už přeskočilo úplně, kdybych zabil jeho ženu.
107) MAX
Ukonči jeho trápení, než ho Tourette sežere zaživa.
108) SAM
Snažíme se mu život zachránit, ne ukončit.
109) SAM
Začíná se nám tu rýsovat dojemná rodinná sešlost, ale nejdřív bychom měli najít tu boxerskou rukavici.
110) SAM
Většina krys by byla na Vánoce z bludiště nadšená, ale tahle krysa chce jen boxerskou rukavici.
111) SAM
Krysy milují bludiště, ale dávat umírajícímu dítěti lstivě zamaskovanou past je kruté, dokonce i na nás.
112) SAM
Veselé Vánoce, paní Dvojzubková. Mame tu pro vás dárek, co každá krysa zbožňuje: nevyřešitelné bludiště!
113) MAX
Pojď si pro to!
114) JIMMY
Vrať se mi zpátky, Mary!
115) MAX
No, už chápu, proč byl Jimmy tak zdrcen ztrátou manželky. Je to poklad.
116) MAX
Zajímalo by mě, kdy Jimmyho opustí žena [smysluplně] Vsadím se, že brzo.
117) MAX
Same, neviděl jsi moje pasti na krysy? No dobře, budeme si muset nějak poradit.
118) MAX
Ooh, uvidíme, jestli můžeme nechat Jimmyho zavřít za sledování TV! Heh. Zelenáč.
119) MARY
[z díry] Jimmy! Přestaň tu vysedávat a získej tu rukavici!
120) MARY
[z díry] Jimmy! Neradím ti, aby ses díval na televizi!
121) MARY
[z díry] Jimmy! Přestaň se dívat na televizi, nebo, přísahám, tě opustím!
122) 
Ramínko
123) 
Mravenčí farma
124) 
Vlasy Bradyho Cultera
125) 
Nástěnka
126) 
Kalendář s přejetými zvířaty
127) 
Maxův stůl
128) 
Dveře do kumbálu
129) 
Krabice na koblihy.
130) 
Staré případy
131) 
Kartotéka
132) 
Zloději a chuligáni
133) 
Obrázek motorky
134) 
Ruka Jesseho Jamse
135) 
Hypno méďa
136) 
Jimmy Dvojzubka
137) 
Lávová lampa
138) 
Leonard
139) 
Vypínač
140) 
Telefon
141) 
Telefonní stolek
142) 
Bující vegetace
143) 
Portál do současnosti
144) 
Krysí díra
145) 
Nejjednodušší bludiště na světě
146) 
Duch minulých Vánoc
147) 
Jimmy Dvojzubka
148) 
Televize
149) 
Timmy Dvojzubka
150) 
Videopřehrávač
151) 
Otevřít okno
152) LEONARD
AAAAAHH!!!! OK, OK! Řeknu ti, kde je ten sendvič, jenom už nech moji mámu na pokoji!
153) MAX
Jistěže, Leonarde, nikdy bychom neudělali nic, co by zneuctilo tvou mrtvou matku.
154) LEONARD
Nikdy jsem ten sendvič neodnesl z kasína. Schoval jsem ho v příhrádce na výhru jednorukého bandity a potom jsem vzal tu jednu ruku, aby nikdo nemohl vyhrát!
155) SAM
Což nás přivádí k další otázce. Kde je ta ruka?
