1) ROBOT
Příšel jsem o ten láskyplný pocit. O-o-o...
2) SAM
Promiňte, {zmateně} pane Obrovská natahovací hračko hromadného {znepokojeně} ničení, pane.
3) ROBOT
Ano?
12) ROBOT
Ano?
4) SAM
{normálně} Předpokládám, že nemáte v úmyslu přestat demolovat naši budovu.
5) ROBOT
Hmmm. Popravdě na tom nezáleží.
6) ROBOT
Existuji pouze jako mašina naprosté zkázy. 
7) ROBOT
To co dělám je to, co jsem, ať už mi na tom záleží nebo ne.
8) MAX
{ustrašeně} Ale ne, Same. On chodil na vyšší odbornou školu!
9) SAM
{důrazně}Je to problém.
10) ROBOT
Karma karma karma karma karma chameleon...
11) SAM
Promiňte, pane Drtitrone.
13) SAM
Mohl bys s tím přestat, prosím?
14) ROBOT
Když mě zapneš, když mě zapneš, tak už nemůžu nikdy přestat.
15) MAX
{zmateně} He? [O čem to mluvíš?]
16) ROBOT
Existuji, abych ničil. Naplňuji jediný smysl své existence.
17) ROBOT
Filozofická otázka tedy nezní "jestli přestanu," ale "MOHU vůbec přestat?"
18) SAM
{ustaraně} A jaká je odpověď?
19) ROBOT
Ne!
20) MAX
{odměřeně} To mě přivádí k otázce: "Přestal bych mu nakopávat ten jeho rezavý kovový zadek {zvažuje}, nebo DOKÁZAL bych přestat?"
21) 
Přestaň! Zlý obří robot!
22) SAM
Byli bychom rádi, kdybys nám přestal demolovat čtvrť.
23) ROBOT
Oslava starých dobrých časů. No tak.
24) 
Prosím přestaň?
25) SAM
{naštvaně} Policie na volné noze! {důrazně} Jsi zatčen!
26) MAX
Polož tu budovu a uklidni se a jenom jednomu z nás se něco stane.
27) ROBOT
Nepodléhám žádné autoritě kromě své mechanizované vůli!
28) ROBOT
Musím jít, kam chci jít, musím dělat to, co chci dělat
29) 
Jsi zatčen!
30) SAM
{sarkasticky}Vážně, jsi zatčen! {normálně} My jsme policie!
31) ROBOT
Všichni poldové v obchodu s koblihami zpívají: Cesto má...
32) MAX
{naštvaně}A dost! Nikdo mi nemůže zpívat popové šlágry a zůstat naživu!
33) ROBOT
Do you really want to hurt me?  Do you really want to make me cry?
34) MAX
{chápavě}Ano, {ďábelsky} a ano.
35) 
Jsme policie!
36) SAM
{důrazně} Nahlásíme Tě! Jaké je tvé sériové číslo?
37) ROBOT
Eight Six Seven Five Three Oh Niiiine.
38) MAX
{smutně}Já už to asi dlouho nevydržím, Same.
39) ROBOT
Měl jsem střet se zákonem a robot vyhrál!
40) 
Nahlásíme tě!
41) 
Mám otázku.
42) SAM
{sarkasticky}Vypadáš jako rozumný člověk...
43) MAX
{nevěřícně} Na robota...
44) SAM
{normálně}Můžeš nám něco vysvětlit?
45) ROBOT
Nemůžete mi položit otázku, kterou bych hned nedokázal zodpovědět. Jen do mě.{vesele}
46) ROBOT
Metaforicky.
47) MAX
{smutně}Awww... [sklíčeně]
48) SAM
Chci se tě na něco zeptat.
49) SAM
Jsme hned zpátky.
50) ROBOT
Prosím, vraťte se do budovy, abych vás v ní mohl rozdrtit!
51) MAX
{happy}To půjde!
52) SAM
Proč tohle děláš?
53) ROBOT
Nemám na výběr. Diktát existencionální filosofie je jasný:
54) ROBOT
Když se objeví problém, musíš ho zvládnout. Odhodlaně.
55) SAM
{zmateně}Kierkegaard?
56) ROBOT
Těsně.  Devo.
57) SAM
Máš svobodnou vůli?
58) ROBOT
Neexistuje nic jako svobodná vůle. Děláme pouze to k čemu jsme předurčeni.
59) MAX
Hej, robote, {vznešeně} měl bys čas dělat mi svědka u mého příštího soudu za válečné zločiny?
60) ROBOT
Budu mít volno hned, jak vás oba zničím.
61) MAX
{vesele}Skvěle!
62) SAM
Může to napravit?
63) ROBOT
Dokáže
64) MAX
Má pravdu.
65) SAM
Co když je všechno iluze a vůbec nic vlastně neexistuje?
66) ROBOT
Tato teorie byla vyvrácena dávnou australskou filozofkou Olivií Newton-John, která pravila "Let's get physical, physical. I wanna get physical."
67) MAX
{nevěřícně}Vše je tak komplikované!
68) 
Co když je to všechno jen iluze?
69) SAM
Dokázal by Bůh stvořit tak velký ledvinový kámen, že ani on by ho nedokázal vypudit?
70) ROBOT
Samozřejmě. Všemohoucí Bůh může, podle definice, stvořit cokoliv si usmyslí.
71) ROBOT
Moje otázka: dokáže někdo vymyslet vtip tak předvídatelný, že nedojde ani vám?
72) MAX
{přemýšlí} To... nelze... vypočítat!
73) 
Je Bůh všemocný?
74) SAM
Proč se hlupáci zamilovávají?
75) ROBOT
Hmmm. Proč ptáci zpívají tak přitepleně? A proč milenci vyhlíží rozbřesk?
76) SAM
Proč, pokaždé když jsi poblíž, se z ničeho nic objeví ptáci?
77) ROBOT
To se děje? Fascinující!
78) ROBOT
Může se vyskytnout tvor, jehož samotná existence záléží pouze na blízkosti pozorovatele?
79) 
Proč, vždy když jsi poblíž, se objeví ptáci?
80) SAM
Kdo napsal knihu lásky?
81) ROBOT
Hmmm. To by mě zajímalo kdo.
82) SAM
Věříš na život po lásce?
83) ROBOT
Hmmm, takhle položenou otázku jsem nikdy před tím neslyšel...
84) 
Žádné další otázky.
85) SAM
Tohle mě už nudí.
