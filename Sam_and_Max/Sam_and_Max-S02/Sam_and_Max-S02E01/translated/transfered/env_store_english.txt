1) SAM
Nazdárek, Bosco!
2) BOSCO
Pšt! Zrovna jsem dostal balíček, který tiká hlastitěji než všechny kardiostimulátory Larryho Kinga dohromady! Tohle bude fakt bomba! Vyhodím to do povětří!
3) SCANNER
Stát!
4) SCANNER
Vítejte v Boscově Poránici! Prosím počkejte chvíli než zkontroluji, jestli jste skutečně vítáni, nebo jen marním svůj čas zbytečnými zdvořilostmi!
5) SAM
Nic nevystihuje "Poránici" lépe než tělesná prohlídka nezbytná k tomu, aby sis mohl koupit žvýkačky a lízátka!
6) SCANNER
Byli jste uznáni za "ne zcela nepřijatelné." Vítejte!
7) SAM
Můžeme si půjčit tvůj odbombovač? Po kanceláři máme poházených pár starých min, kterých se potřebujeme zbavit!
8) BOSCO
Ne! Kdybych svoje zařízení na likvidaci bomb půjčil každému v okolí, kdo je potřebuje, tak bych ho už nikdy neviděl!
9) SAM
Pokud mluvíme o naší čtvrti, tak je to vlastně pravda.
10) SAM
Rychlé, zábavné a třaskavé zařízení na uchování nebezpečných látek, pro celou nukleární rodinu.
11) SAM
Najednou to všechno dává smysl!
12) MAX
Nechápu jak do toho spadáme my, Same.
13) BOSCO
Oh, nahoře mám pár zdí zasvěcených jenom vám dvěma.
14) SAM
To je starý zvětšovač fotografií.
15) MAX
[ďábelsky] Zajímalo by mě, jestli to dokáže zvětšit i něco jiného...
16) SAM
Ne, Maxi.  Ne.
17) BOSCO
Drž se dál od mého kryogenického mrazáku! Až nastane můj čas, mám v úmyslu se tam uvést do stavu hibernace!
18) SAM
Pokud je to kryogenický mrazák, proč tam máš nanuky upatlané od hořčice?
19) BOSCO
To že bude ve stavu hibernace ještě neznamená, že nebudu mít chuť na svačinu!
20) SAM
Boscův krygenický odchod do důchodu.
21) SAM
I v temné komoře musejí být nějaké hračky.
22) SAM
Alespoň, že tenhle odpadkový koš je pořád ta samá, neškodná, smetím ověnčená nádoba, jak ji známe. 
23) BOSCO
Hej! Jdi pryč od mého virového prionického malárického očkovadla!
24) SAM
Oh, dobře.
25) SAM
Boscovo úložiště životu nebezpečného odpadu.
26) SAM
[rozvážně] Klobásové jerky.
27) MAX
Z čích klobás vyrábějí ty jerky, Same?
28) SAM
Nejsi dost starý, abys to věděl. Já taky ne.
29) SAM
Týdeník Maxovy obžaloby, část 1, 2, a 3.
30) MAX
Část 4 vychází příští týden!!
31) SAM
Nyní konečně můžeme potvrdit kolik přesně různých kmenů bakterií žije v nakládané pochoutce.
32) MAX
Pamatuj, že mám v kanceláři stále 163. 
33) SAM
Jsem rád, že se někdo má na pozoru před nevyhnutelným povstáním holubů.
34) SAM
Fotografické vybavení pro lidi, kteří nikdy neslyšeli slovo "digitální".
35) SAM
Kde získal tyhlety záběry?!
36) SAM
Geniální plán Hugh Blisse. Bosco trochu zaspal dobu.
37) SAM
Hele, Bosco, my se ti o ten balíček postaráme.
38) BOSCO
Zpátky, hochu! Tohle je moje.
39) SAM
Teď Maxi, dokud se nedívá, seber ten balíček a padáme... 
40) MAX
Hele Same! To je extra velká figurka hladomoru s opravdovým projímavým účinkem!
41) SAM
Čekaní na otevření dárků ti nikdy moc nešlo, kamaráde!
42) MAX
Má to přes 1700 artikuláčních bodů, z toho 250 jenom v čelistech!
43) BOSCO
Počkat, kde je můj balíček? Dobrý Bože, *oni* ho vzali. A Sama a Maxe vzali taky!
44) BOSCO
Žádné místo není v bezpečí!
45) SAM
Tohle je ten typ rádia, které si ostatní rádia dává k snídani.
46) BOSCO
Nezahrávej si s tím!
47) SAM
Jsou tu ty vysoce výkonné bezpečnostní lasery, aby nám znemožnily vstup na záchod... nebo proto, aby něco udržely uvnitř?
48) SAM
Všechno je stabilní... tedy, až na majitele.
49) SAM
Podíváme se. Bosco šmíruje... svoji koupelnu, ředitele televize WARP, nadzemku v Chigacu, a... tak moment!
50) SAM
Hele, rentgen!
51) SAM
To vypadá jako obsah Boscova balíčku.
52) SAM
Hele, to vypadá jako jedna z těch akčních figurek jezdců!
53) BOSCO
Jděte ode mě!
54) BOSCO
[Normálně] Hej!!
55) BOSCO
[Normálně] Aaah!
56) BOSCO
[Normálně] Přestaň!
57) BOSCO
[Normálně] Pozor!!
58) BOSCO
[Normálně] Dej to pryč!
59) BOSCO
[Normálně] Co to deláš!
60) BOSCO
[Normálně] Je to šílenec!
61) BOSCO
[Normálně] Všechny nás zabije!
62) BOSCO
[Normálně] Všechny nás zabiješ!
63) BOSCO
[Normálně] Nenuť mě si tam dojít!
64) BOSCO
[Normálně] Má zbraň! A neumí s ní zacházet!
65) BOSCO
[Normálně] Proč bych se kdy zbavoval B-TADS?!
66) 
Odbombovač
67) 
Bosco
68) 
Teorie všeho
69) 
Východ
70) 
Zvětšovač
71) 
Lednice
72) 
Mrazák
73) 
Odpadkový koš
74) 
Ohřívač klobás
75) 
Časopisy
76) 
Mikroskop
77) 
Periskop
78) 
Fotografické vybavení
79) 
Fotky
80) 
Mistrovský plán
81) 
Balíček
82) 
Rádio
83) 
Bezpečnostní rám
84) 
Seismograf
85) 
Detailní fotky
86) 
Rentgen
87) 
Rentgenové vidění
