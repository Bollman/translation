1) SAM
Hele, moje sněhová koule roztála.
2) MAX
Už mě to unavuje, Same.
3) MAX
Tohle je ztráta času.
4) MAX
Marníš můj život, Same.
5) MAX
Jdeme, Same!
6) MAX
Tohle místo me nudí.
7) MAX
Tohle místo mi už leze krkem. Jdem dál.
8) MAX
Same, jakožto mezinárodní vyslanec cítím, že Santovým skřítkům dlužíme výslech pěkně po americku.
9) MAX
Hlasuji pro to toho skřítka znovu rozbrečet!
10) MAX
Musím říct, že přivést toho nevinného skřítka na pokraj sebevraždy byl nejsvětlejší moment dne.
11) MAX
Přál bych si mít trochu těch skřítčích slz, když jsme bojovali s Drtitronem!
12) MAX
Velký strom by se hodil.
13) MAX
Zapomeň na Santu. Měli bychom skákat do komínů a nic netušící děti vystrašit k smrti!
14) SAM
Mmmm.  Ne.
15) MAX
V Santově pokoji musí být NĚCO, co by nám mohlo pomoct
16) MAX
No tak, někdo v továrně na hračky přece musí vědět, kde bychom mohli najít nějaké ty akční figurky čtyř jezdců.
17) MAX
Zajímalo by mě co dostali od Santy ostatní lidé z naší ulice. Vsadím se, že obřího bojového robota nic nepřekoná...
18) MAX
Same, můžeme jít, prosím, tancovat na mršině toho obřího robota? Slíbil jsi to!
19) MAX
Byla to zábava a tak vůbec, ale teď už bych se rád vrátil do Santovy dílny a pořádně to tam vyraboval, chápeš?
20) MAX
Jedna věc mě nikdy neomrzí a to je mlácení Jimmyho! Musím rychle shodit 97 liber, abych se dostal do jeho váhové kategorie.
21) MAX
Víš, co potřebujeme? Panenku Burgess Meredith na dálkové ovládání, aby naučila Boxující Betty bojovat jako chlap. Ženská verze chlapa.
22) MAX
Děsivá myšlenka pro dnešní den: díky Drtitronovi jsme teď s Boscem sousedé!
23) MAX
Už se nemůžu dočkat, až uvidím Boscův odbombovač v akci!
24) MAX
Musím si udělat seznam věcí, které chci vyhodit do povětří. To nějaký čas zabere.
25) MAX
Podle mě, Bosco potřebuje něco, co odpoutá jeho mysl od toho balíku. Dřív než mu úplně bouchnou saze.
26) MAX
Jestli nám Bosco toho jezdce rychle nedá, tak mu opravdu pošlu tikající zásilku!
27) MAX
Mimochodem, myslíš, že v tom dárkovém zařízení v Santově dílně mají nějaké bomby?
28) SAM
Hmm.
29) MAX
Hele, nebylo dnes U Stinky velké znovuotevření? Nemůžu se už dočkat další lákavé otravy jídlem!
30) MAX
Já jen vím, že falešná Stinky něco skrývá!
31) MAX
Hele, můžeme si zahrát u Stinky hospodský kvíz? Nikdy jsm se necítíl tak hospodsky.
32) MAX
Hele, všiml jsi si toho nového podniku vedle Stinky? Jmenovalo se to nějak... Vytuň káru, nebo tak nějak?
33) MAX
Hele, pojďme si znovu promluvit s P.O.L.I.Š.I. U Pasákovy káry.
34) MAX
Same, prosím, zahrajeme si ještě Výzvu řízení pasákovy káry!
35) MAX
Tolik panenek Umuč si Elma na zlikvidování a tak málo času.
36) MAX
Koukni, soška jezdce na kapotě našeho auta se pozoruhodně podobá figurce jezdce, kterou potřebujeme na vymítání!
37) MAX
Nemáš někdy pocit, že jsme toho dnes od Santy zkrátka nenakradli dost?
38) MAX
Na jednu věc jsem ale pořád ještě nepřišel: k čemu je v Santově dílně gramofon???
39) MAX
Čas vymítat! Musíme ty jezdce rozhodit okolo severního pólu.
40) MAX
Pojď, Same, musíme se vrátit do Santovy dílny a dokončit vymítání!
41) MAX
No tak. Najdeme nějakého toho ducha Vánoc a přejdeme k očiňování.
42) MAX
Kdeže jsme to viděli Jimmyho šťastnou boxerskou rukavici? Ó ano, měl ji Jimmy!
43) MAX
Same, myslím si, že dokážu Jimmyho přemluvit, aby slezl z té římsy. Jsem profesionál.
44) SAM
*kucká*[jako by říkal, 'ano, určitě']
45) MAX
Můžeme se vrátit do minulých Vánoc? Nemám na budoucnost zrovna ty nejlepší vzpomínky.
46) MAX
Ostuda, že jsme Jimmyho nepřemluvili, aby slezl z té římsy. Sakra, žena ho opustila, že je takový ubožák. Co můžeme dělat?
47) MAX
Jsem zvědav, co na to Jimmy řekne, až se dozví, že ho žena opustila pro tvoji kapsu.
48) MAX
Koho zajímá záchrana minulých Vánoc? Říkám, sebereme boxerskou rukavici a vzůru do Mexika!
49) MAX
Možná že k tomu, abychom ty brouky odtud odehnali, stačí říct BAF!
50) MAX
Hele, vyléčil jsem si škytavku!
51) MAX
Možná bychom si měli s Broukem promluvit o jeho rodině dokud nás ještě slyší.
52) MAX
Ti brouci chtějí znamení od svého otce? Jó, ta úklidová četa se postarala o to, že to není moc pravděpodobné.
53) MAX
Chlape, kdybych věděl jak otec těch brouků zněl, opravdu bych si pohrál s jejich myslí!
54) MAX
Hej, Same, nacpeme tomu broukovi do obličeje karbanátky a uvidíme, jestli začne mluvit jako ta Hračkářská mafie!
55) MAX
Nudím se. Hele, zkusíme toho brouka přimět, aby si vzpomněl, jak jeho táta koupil farmu!
56) MAX
Tohle bude pro všechny zábava.
57) MAX
Smůla, Stinky nikdy neslyšela termín "čistící prostředky", protože jejich pach by toho brouka určitě nadzvedl ze židle!
58) MAX
Zdá se mi to, nebo si Santovy sáně vyloženě říkají o vyjížďku?
59) MAX
Víš, když jsem viděl Santův nacpávač ponožek, tak to bylo poprvé, co jsem si přál nosit ponožky!
60) MAX
I mizerní lháři dostanou alespoň to uhlí!
61) MAX
Same, požaduji radost z vlastnictví ponožky nacpané Santou! I kdyby ten obsah ponožky byl pro někoho jiného.
62) MAX
Když o tom přemýšlím, ponožka plná uhlí by teď byla přesně to pravé.
63) MAX
Musíme najít někoho, kdo lže jako když tiskne a zároveň nosí ponožky.
64) MAX
Víš, řekl bych, že bychom se měli pokusit získat ponožku Stinky, ale jestli jsem se o randění něco naučil, tak to, že nikdy od ženy nemáš požadovat její ponožky.
65) MAX
Nechápu, jak Bosco a Stinky zvládnou stát celý den. Mě strašně bolí nohy.
66) MAX
Ačkoliv si myslím, že Stinky je odporný lhář, snad by ocenila malý dárek, slyšet nás říkat 'díky za tu ponožku, kterou se nám chystáš dát'.
67) MAX
Kruci, doufám, že si Stinky užije tu nožní lázeň, kterou jsme ji tak pracně sehnali.
68) MAX
Cítil jsi tu ponožku, co si Stinky sundala? Možná, že JE opravdová Stinky! Rrrank!
69) MAX
Co hodláš se Stinkynou ponožkou dělat? Pověsit si ji nad krb?
70) MAX
Řekl bych, že ponožka plná uhlí je úplně na nic. No tak, vážně, co dneska ještě funguje na uhlí?
71) MAX
Hele, víš, kdo by byl opravdu nadšený z lahve plné duchů Vánoc? Démonické želé.
72) SAM
Píše se tu, že na vymícení démona je potřeba:
73) SAM
Nasbírej 4 všechny akční figurky jezdců z apokalypsy...
74) SAM
Polož jezdce okolo magnetického pole...
75) SAM
Pak vyžeňte démona pomocí písně přátelského démona.
76) SAM
Na polačení démona budeme potřebovat pomoc duchů Vánoc.
77) SAM
Boxovací hračka na dálkové ovládání.
78) SAM
Jimmyho šťastná boxovací rukavice.
79) SAM
Tohle ani nemá cenu komentovat.
80) SAM
Tohle můžeme použít, abychom poslali poštu přátelům.
81) SAM
Tenhle pohled se mi opravdu nikdy neomrzí.
82) SAM
Jezdec smrti.
83) SAM
Jezdec hladomoru.
84) SAM
Jezdec moru.
85) SAM
Jezdec války.
86) SAM
Naštěstí pro nás oba se mi Jimmyho žena pohodlně vejde do kapsy.
87) SAM
Nejjednodušší bludiště na světě. Pro děti od 3 do 6 let.
88) SAM
Nahrávka satanské vánoční hudby, včetně písně přátelského démona!
89) SAM
Vybělená sněhová koule.
90) SAM
Stinkyna ponožka.
91) SAM
Stinkyna ponožka je plná uhlí.
92) SAM
Domov tří duchů Vánoc.
93) SAM
Musíme zvítězit nad všemi duchy, abychom zastavili démona.
94) SAM
Uvnitř je jen jeden duch. Musíme zvítězit nad dalšími dvěma, abychom zastavili démona.
95) SAM
Uvnitř jsou jen dva duchové. Musíme zvítězit i nad tím posledním, abychom zastavili démona.
96) SAM
Všichni tři jsou uvnitř, takže je čas je vypustit na seňora Démona!
97) SAM
Skrítčí slzy pomohly tomu stromu vyrůst.
98) SAM
Nic zvláštního.
99) SAM
Nuda.
100) SAM
Raději bych si prohlédnul neco zajímavého.
101) SAM
Nahh.
102) SAM
Ani náhodou.
103) SAM
Nuh unh.
104) SAM
Ne pane.
105) SAM
Ani náhodou!
106) SAM
Nic to nedělá.
107) SAM
Nic se neděje.
108) SAM
Nezastřelil bych svého malého kamaráda!
109) SAM
Raději ne.
110) SAM
Nemyslím si, že tohle můžu poslat poštou.
111) SAM
Použitelné prakticky jenom v gramofonu.
112) SAM
Myslím, že si svou vybělenou sněhovou kouli nechám na zajímavější cíl.
113) SAM
Nechci začínat válku, kterou nemůžu vyhrát.
114) SAM
Dostat zásah na tuhle vzdálenost vybělenou sněhovou koulí by mohlo způsobit trvalou slepotu.
115) MAX
A?
116) SAM
Obyvkle jsem ochten akceptovat je dočasnou slepotu.
117) SAM
To nemůžu zalévat.
118) 
Sam
119) 
Max
