1) SAMKID
[young] Hey Max. Max. Max. Max. Max. Max. Max.
2) MAXKID
{angry}[young] [annoyed] WHAT?
3) SAMKID
{happy}[young] Nothin’.
4) SAMKID
{confused}[young] What are we doing here, Max?
5) MAXKID
{sad}[young] I don’t know. I’m bored.
6) SAMKID
{normal}[young] You want to color, or find a way to make you High Priest of the Ocean Chimps?
7) MAXKID
{confused}[young] Can’t we do both?
8) SAM
What is it with mysterious portals lately?
9) MAX
Sybil’s not that mysterious once you get to know her.
10) SAM
I don’t even want to know what you think I meant.  Anyway, we should probably save her from the rampaging triangle thingy.
11) MAX
Yeah, we might need her later.
12) 
I guess it’s up to us to save Sybil.
13) SAM
Somehow I always thought the Bermuda Triangle would be a lot bigger.
14) MAX
You also thought that about bluegrass {happy}music, but it doesn’t make it so.
15) SAM
{stern}Never mock bluegrass, lest I flatten your Scruggs.
16) MAX
{normal}Is that Sybil I hear shrieking?
17) SAM
{normal}Oh yeah!  We should probably try to help her out before it gets dark.
18) 
That’s a Bermuda Triangle?
19) SAM
You know what I like about our neighborhood?
20) MAX
No volcanoes?
21) SAM
Exactly.
22) 
It’s good to be home.
23) SAM
You’re still the President, right? {confused} Can’t you get an urban renewal squad down here to fix the pavement?
24) MAX
I don’t sully myself with local matters.  They’re bad for the skin.
25) 
Is that sidewalk crack getting bigger?
26) 
Let’s go.
27) SAM
Let’s get cracking.
28) MAX
Certainly.
29) MAX
Where are we going, Sam?
30) SAM
Let’s play that game with the COPS.
31) 
Let’s play Bagpipe Obliteration Ultimate!
32) SAM
Nowhere in particular. Let’s just drive.
33) 
Let’s just drive around.
34) 
Never mind.
35) SAM
Eh, never mind.
38) 
Nowhere, I just want to admire our decals.
