1) SAM
Howdy, COPS! How’s the auto body business?
2) MOVIEFONE
Hello! We’re working on ... [cut off]
3) SYBIL
{scared}Aieeeeeeeeeeee!
4) MOVIEFONE
Hello! We’re working on the most... [cut off]
5) SYBIL
Heeeelllllllllp!
6) SINISTAR
GEEZ CALM DOWN LADY!
7) MOVIEFONE
We’re sorry. We cannot handle your request until you’ve taken care of the noise problem.
8) SAM
Hey, COPS!
9) SAM
Howdy, COPS! Anything new in the auto body business?
10) MOVIEFONE
Do you want to have ladies love you and men want to be you?
11) MAX
Naturally!
12) MOVIEFONE
Make that dream a reality with our BRAND NEW CAR (horn)!
13) SYNTH
With a captivating melody composed by our resident musical maestro.
14) SINISTAR
OMFG, I WANT ONE!
15) SAM
All right, enough with the sales pitch. How much?
16) SYNTH
It is not for sale.
17) SINISTAR
DENIED!
18) MOVIEFONE
We’re sorry.
19) MOVIEFONE
The Synth-o-Tronic Audio Enhancer is available exclusively to beta testers of our new augmented reality game.
20) SAM
Hey, COPS, what’s new?
21) SINISTAR
NEW IS OVERRATED!
22) SAM
Howdy, COPS!
23) SAM
How’ve things been in the neighborhood?
24) SINISTAR
NOISY!
25) SYNTH
There has been a dramatic increase in folk instrument-related automobile accidents.
26) SAM
Oh right, the 10,000-man Scottish Polka March is in town this week.
27) MAX
{happy}My favorite time of year!
28) MOVIEFONE
The streets are filled with accordions and bagpipes stacked like so much cordwood!
29) SINISTAR
WE HATE BAGPIPES!
30) 
What’s new around here?
31) SAM
We’d like to pimp out our car.
32) MAX
We want some {aloof}18-inch Blitz type 4’s, a 6-valve PTX chrome intake, double-injected Gunther regulators on the back 10, and some neon on the undertow that flashes morse code sayin’{happy} “Check it out, beeeeeyotch!”
33) SYNTH
All we have is the car horn.
34) MAX
Sweeeet.
35) 
We want to pimp our car.
36) SAM
We’d like some of those decorative decals.
37) MOVIEFONE
Simply destroy rare objects on the road, and if we deem them worthy, we shall award you a commemorative decal free of charge!
38) 
Got any more decals?
39) SAM
Tell us about this new game you’re working on.
40) BLEEPS
Bloop bleep bleep!
41) SYNTH
That is correct. It will revolutionize the entire industry.
42) MOVIEFONE
A music-based videogame!
43) SINISTAR
BOOM!
44) SYNTH
We are sorry. Did we just blow your mind?
45) SAM
Sounds fun! I don’t like to brag,{happy} but my prowess at Banjo Legend Extreme is pretty renowned throughout the tri-state area.
46) MAX
{realizing}And I’ve been banned from 27 local arcades for playing Forbidden Dance Insurrection in ways the designers never intended.
47) BLEEPS
Bloop? [inquisitive]
55) BLEEPS
Bloop? [“what?”]
48) SYNTH
Does not compute. You are saying that music videogames already exist?
50) 
Tell us about your new game.
51) SAM
{confused}How do we play this game of yours?
52) MOVIEFONE
In a world where folk music instruments litter the mean streets, your mission is clear: destroy all the bagpipes.
53) SINISTAR
BAGPIPE OBLITERATION ULTIMATE!
54) SAM
Don’t we have to destroy the bagpipes at the right time, earning more points the closer we get to being in rhythm with the soundtrack?
56) SINISTAR
THAT’S WAY TOO COMPLICATED!
57) SYNTH
Calculating that would require processing power far beyond... Error: Not a Number.
58) MOVIEFONE
Just destroy all the bagpipes to win our exclusive new car horn!
62) MOVIEFONE
Just destroy all the bagpipes to win our exclusive new car horn!
59) SYNTH
Would you like to play our game? [“War Games” style]
60) 
Can we play your game?
63) 
Can we play your game?
61) SAM
Explain your music game one more time.
64) SAM
We’d like to play your game again.
65) MOVIEFONE
Obliterate the bagpipes to win fabulous prizes!
66) SAM
With my driving skills and {worried}your... dead weight,{normal} we’re going to have the most tricked-out car in the neighborhood!
67) MAX
I just like to win things.
68) 
We want to play your game again.
69) SAM
We want to play your game again.
70) SAM
{confused}Do you guys know anything about Easter Island?
71) SYNTH
Accessing... Easter Island was founded in 1914 by former members of the 80s progressive rock band Asia.
72) MOVIEFONE
Located off the southwest coast of your mom, the island is considered by many to be the birthplace of television personality Ryan Seacrest as well as American jazz music.
73) SAM
{incredulous}I see you’re still doing all your research on the internet.
74) SINISTAR
INFORMATION WANTS TO BE WRONG!
75) 
Know anything about Easter Island?
76) SAM
{confused}Do you know anything about missing persons?
77) SYNTH
Have you tried looking on Easter Island? Most end up there.
78) 
Seen any missing persons?
79) SAM
How do you stop a rampaging volcano god?
80) BLEEPS
Bleep bloop bleep bleep bloop!
81) MOVIEFONE
I love that joke.
82) SINISTAR
GETS ME EVERY TIME!
83) 
So long.
84) SAM
See you, COPS. Keep a song in your hearts.
85) SYNTH
Whatever.
86) SAM
See you, COPS.
87) SAM
Let’s do it!
88) SAM
Not right now.
