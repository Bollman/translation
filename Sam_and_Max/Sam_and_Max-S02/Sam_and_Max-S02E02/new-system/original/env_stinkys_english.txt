1) STINKY
[mumbling] ...out, dammit... stupid spot...
2) SAM
Hiya, Stinky!
9) SAM
Hiya, Stinky!
3) STINKY
Oh, it’s you guys. Hank and Jethro, was it?
4) SAM
Sam and Max, actually.
5) MAX
[random, cheerful interest] What are you doing?
6) STINKY
[defensive] What? I was just taking care of some... cleaning. [last word said with mysterious emphasis]
7) MAX
If that’s a blood stain, you should be sure to use... bleach. [last word said with mysterious emphasis]
8) STINKY
[defensive] I don’t know what you’re talking about!
10) STINKY
[surprised] What? [calmer] Oh.
11) SAM
I don’t know which is scarier: the idol or the cowboy hat he’s wearing.
12) SAM
Stinky’s has a “no smoking unless you’re Flint” policy.
13) MAX
Sounds a lot like their policy on homicides.
14) SAM
This your pipe, Stinky?
15) STINKY
Yes, I looted it from the corpse of a 50 foot Sherlock Holmes.
16) SAM
That’s one ugly looking Buddha.
17) MAX
Correction. That’s TWO ugly looking Buddhas.
18) SAM
I don’t think we’ll need that until global warming gets much, much worse.
19) SAM
We can use this to look into our future.
20) MAX
Cool! What do you see?
21) SAM
Nothing. I guess we haven’t got any future.
22) MAX
Oh well. It’s been fun.
23) SAM
Where’s the band?
24) STINKY
I keep hiring drummers, but they keep combusting.  And not slowly, over time either... spontaneously!
25) SAM
The fedora never goes out of style. Or further out of style.
26) SAM
Look, Max, it’s our neighbor, Flint Paper!
27) FLINT
Hiya, fellas. Keep it on the down-low, would ya?
28) FLINT
I’m doing surveillance.
29) MAX
Oh boy! Real detective work!
30) SAM
Who are you spying on?
31) FLINT
Some mug name of “Bosco.”
32) SAM
Why are you checking out Bosco, Flint?
33) FLINT
For a client, Sam-o. Said she was the poor chump’s mom.
34) FLINT
She was a real no-nonsense dame. With legs up to here.
35) FLINT
Hairy legs, but still...
36) SAM
How goes the stakeout?
37) FLINT
Gettin’ interesting, fellas. Bosco may be on the move.
38) MAX
[excited] Yeah?
39) FLINT
He’s acting awful suspicious.
40) MAX
[let down] Oh. That’s just Bosco.
41) SAM
How’s the stakeout going, Flint?
42) FLINT
Shhhh! This is make-or-break time, boys.
43) FLINT
He could high-tail it outta here at any second.
44) SAMKID
[young] Hey, Mr. Paper. Whatcha doin?
45) FLINT
Trust me, kid. You don’t wanna get inside Flint Paper’s world. I’ve SEEN things.
46) MAXKID
[young] [whispered, to Sam] He means naked ladies!
47) FLINT
Stay in drugs, don’t do school. Ahh, I dunno. Just leave the grown-ups alone, capiche?
48) SAMKID
[young] Flint Paper is material not suitable for younger audiences.
49) SAM
It’s a gong with no gonger.
50) MAX
Where’s Jaye P. Morgan when you need her?
51) MAX
How come jackalopes get to grow antlers but I can’t?
52) SAM
Millions of years of evolution, I guess.
53) MAX
Who’s got that kind of time?
54) SAM
Jackalopes, apparently.
55) SAM
Max’s evolutionary superior.
56) SAM
I don’t have any quarters.
57) STINKY
No problem! The jukebox only takes twenties.
58) SAM
It’s locked.
59) MAX
Or someone’s holding it shut from the inside.
60) SAM
I love your optimism, Max.
61) SAM
It doesn’t need a gardener; it needs a plastic surgeon.
62) SAM
What did Clark Gable order?
63) STINKY
Six buckets of pomade and a side of fries.
64) SAM
Peace, love, and all that hoohah - Gandhi.
65) SAM
Hey, Gabe Kaplan!  Whatever happened to him?
66) MAX
He’s been masquerading as Gene Shalit apparently.
67) SAM
Who is this J.H.C. fellow?
68) SAM
Keep on truckin’, Love Mother T.
69) SAM
Not pictured: his right hand.
70) SAM
You know, we run into Whizzer all the time.
71) STINKY
I’m sure you do.  [under breath] Mangy lying dog.
72) SAM
Looks like somebody already had a slice.
73) MAX
That explains all those ambulances this morning. And the screaming.
74) SAM
West Dakota.
75) SAM
Whizzer’s really made his mark.
76) SAM
The Philippines.
77) SAM
Come for the cockfighting, stay for the chicken.
78) SAM
[over-enunciated] Lake Titicaca.
79) SAM
That can’t be real.
80) SAM
He looks like kind of a wuss. For a shark.
81) MAX
Maybe that’s why he’s hanging on the wall at Stinky’s now. Instead of prying open a diving cage in the South Pacific.
82) SAM
It’ll take more than that to turn this restaurant around.
83) SAM
Can we borrow your sign, Stinky? It’s kind of important.
84) STINKY
Well, okay. But bring it back, it’s a national heirloom.
85) STINKY
That’s the stop sign that would’ve prevented the Kennedy Assassination if Connally hadn’t run it.
86) SAM
Stop!
88) SAM
Stop.
87) SAM
They’re nice snowshoes, but I wear elevens and those look like a size ten and a half.
89) MAX
But what if I don’t feel like stopping?
90) SAM
Then our insurance rates go up again.
91) SAM
Sadly, I think my days of pillaging coastal villages are now behind me.
92) SAM
I never really expected that to work!
93) MAX
Paranormal entities are dumb and gullible.
94) SAM
Satisfying!
95) SAM
Hmmm, nothing happened.
96) SAM
[thinking out loud] Why doesn’t Stinky’s gong work like the ones on Easter Island?
97) SAM
I can’t reach it.
98) STINKY
Um, what are you guys doing?
99) SAM
Err... Red improves digestion.
100) MAX
Don’t try to argue with feng shui, Stinky. It’s SCIENCE.
101) MAX
There’s so much great stuff in here!
102) STINKY
Quit eyeballing my decor.  I’ve got alarms.  With lasers.
103) MAX
What’s an octagon, Sam?
104) SAM
Something with eight sides.
105) MAX
Like Parliament!
106) MAX
It takes too long to get here from everywhere else.  Isn’t there a shortcut?
107) MAX
Kid’s meal!  Kid’s meal!  Kid’s meal!
108) MAX
I wish those triangle thingies went everywhere.
109) MAX
Just walking in here from the street outside makes me remember that rabbits have an abbreviated life span.
110) MAX
What’s wrong with Stinky’s gong, anyway?
111) MAX
Maybe there just aren’t any triangle thingies that are the right color.
112) MAX
{sad}I’m cold.{normal}  Let’s go back to the island!
113) 
African Idol
114) 
Alphorn
115) 
Buddha
116) 
Canoe
117) 
Crystal Ball
118) 
Djembes
119) 
Exit Door
120) 
Fedora
121) 
Flint Paper
122) 
Gong
123) 
Jackalope
124) 
Jukebox
125) 
Mummy Case
126) 
Palm Tree
127) 
Clark Gable
128) 
Gandhi
129) 
Gene Shalit
130) 
Jesus
131) 
Mother Theresa
132) 
Napoleon
133) 
Whizzer
134) 
Pie
135) 
Poster
136) 
Shark!
137) 
Ship Wheel
138) 
Stop Sign
139) 
Snow Shoes
140) 
Specials
141) 
Stinky
142) 
Stoplight
143) 
Bermuda Triangle
144) 
Viking Helmet
145) 
Ashtray
147) SAM
Not right now.
