1) SAMKID
[young] Hello, Miss Stinky.
2) STINKY
What do you boys want? {pretentious}Do you have money to pay?
3) MAXKID
{angry}[young] Drop the attitude, lady, I’m the Presi...
4) SAMKID
{stern}[young] Max!
5) MAXKID
{sarcastic}[young] ...President’s adorable nephew here for a visit.
6) STINKY
Well... since I know the President personally, I guess it’s okay.
7) STINKY
What’s your order?
8) SAMKID
{sad}[young] Have you seen our parents?
9) MAXKID
{sad}[young] I gotta make doody real bad!
10) SAMKID
{sad}[young] I heard them laughing as they started the car.
11) MAXKID
{realizing}[young] Can we live with you now?
12) STINKY
Stinky’s is closed, kids.{pretentious} Go run and play in traffic.
13) SAMKID
{angry}[young] You’re a mean old lady!
14) STINKY
Get lost.
15) SAM
{confused}What are you cleaning up? {suspicious}Is that a blood stain?
16) STINKY
[defensive] Don’t be ridiculous! Blood!
17) STINKY
[defensive] You act like I killed Stinky or something! Grandpa. Grandpa Stinky.
18) MAX
{realizing}Oh boy! Intrigue!
19) STINKY
This is just cherry... marinara sauce. For the meatball... banana split... pie... {happy}Italiano.
20) SAM
[less suspicious] Well, that does sound tasty...
21) 
Is that a blood stain?
22) SAM
{confused}Whatever happened to the original Stinky, anyway?
23) STINKY
He retired. Went on an around-the-world cruise.
24) SAM
{incredulous}Stinky hated traveling! {stern}And water!
25) MAX
{stern}And the world!
26) STINKY
Yeah well he must’ve changed his mind.
27) STINKY
[quickly changing the subject]{pretentious} So are you two going to order anything?
28) 
What happened to the old Stinky?
29) SAM
{confused}What’s Flint Paper doing here?
30) STINKY
I was about to ask you the same thing. He’s been here all morning.
31) MAX
{suspicious}Nervous about having a trio of ace detectives snooping into your little scheme, lady?
32) STINKY
{happy}Don’t be silly. I just have to move some{pretentious}... things... out of the meat locker.
33) STINKY
{normal}But I can’t if I keep having to refill his “java” and empty his ashtray.
34) SAM
Next time you hear from him, tell “Grandpa” Stinky we said hello.
35) STINKY
Oh, I don’t think we’ll be hearing from him... ever again.
36) MAX
{suspicious}Because you sent him on a... {aloof}permanent vacation?
37) STINKY
No, because he was a cheapskate and I refuse collect calls.{pretentious} What is it with you guys?
38) 
Tell “Grandpa” Stinky we said hello.
39) SAM
{worried}A rogue triangle is chasing after Sybil!
40) STINKY
Triangle? Yeah, that’s not really my thing. {pretentious}I specialized in the more circular interdimensional portals.
41) MAX
Some expert!
42) SAM
{normal}Yeah, Stinky. {sarcastic}You’re an “expert” in portals like President Max is an expert in diplomacy.
43) MAX
{regal}Risk is more my thing.
44) 
Watch out for Triangles!
45) SAM
Any ideas to help Sybil?
46) STINKY
{pretentious}Drop that Lincoln guy like a bad habit.
47) SAM
We meant more short-term, but thanks.
48) 
Sybil needs our help!
49) SAM
Have you ever been to Easter Island?
50) STINKY
Oh sure. {happy}I lived there for a few years when I was growing up.
51) STINKY
My au pair was surfing legend Miki Dora. Taught me everything I know.
52) 
Ever go to Easter Island?
53) SAM
What can you tell us about Easter Island?
54) STINKY
It’s kind of boring, actually. There used to be a tiki bar near the fountain that was pretty good.
55) STINKY
{pretentious}Terrible service, though.
56) 
What do you remember about the island?
57) SAM
What else can you tell us about Easter Island?
58) STINKY
Don’t drink the water.
59) 
Anything else we should know?
60) SAM
What’s the special today?
61) STINKY
Oh, it’s just a sandwich I whipped up for the Camp David Middle East Peace Summit in 2000.
62) STINKY
Roast beef, munster cheese, and a slab of granite on sliced sourdough.{pretentious} Nothing special.
63) SAM
{confused}A slab of granite?
64) STINKY
{normal}Yeah, you know, for texture. On the kids’ menu I substitute a slab of basalt.{pretentious} It’s easier for the little ones to digest.
65) 
What’s the special?
66) SAM
We’re ready to order.
68) SAM
Bye, Stinky.
69) SAM
We’d like the special.
96) SAMKID
[young] We’d like the special.
70) STINKY
Sure. Granite sandwich, coming right up.
71) MAX
I wanted the other special!
72) STINKY
Sorry, that’s for kids only. {happy}FDA regulations.
73) 
Give us the special.
74) SAM
Max would like the kids’ special.
75) STINKY
That’s for kids only.
76) MAX
{realizing}But I have a childlike sense of wonder!
77) STINKY
Hey, I don’t make the rules.
78) 
Give Max the kid’s special.
79) SAM
We want the green salad, extra smoky, with a side of cracked nuts.
80) STINKY
Sal! The Jack Nicholson, with Dennis Hopper and Gary Busey!
81) 
Green salad with cracked nuts.
82) SAM
Could we have the grilled cheese with tabasco, and a side of curly fries?
83) STINKY
Sal! I need Diane Sawyer with a flamethrower, hopped up on goofballs!
84) MAX
{happy}Oooo, so do I!
85) 
Grilled cheese with tabasco.
86) SAM
We’d like the soft-boiled eggs on waffles, with coffee, extra cream.
87) STINKY
Sal! Get me Scooter Libby on a Glockenspiel with Mike Tyson singing alto!
88) 
Soft-boiled eggs on waffles.
89) SAM
I’ll have the deep fried devil dogs with a diet drink and Max’ll have the salad plate with extra blue cheese.
90) STINKY
Sal! Half a Jack Black with a supersized Keira Knightley! In deep denial!
91) 
Deep fried devil dogs.
92) 
Never mind.
116) SAMKID
[young] Never mind.
93) SAM
On second thought, {confused}I’m not that hungry.
94) STINKY
If you say so.
97) STINKY
You’re in luck, boys. I’ve got one already made.
98) STINKY
One basalt sandwich coming up!
99) MAXKID
{sad}[young] Aren’t you going to cut the crusts off?
100) STINKY
I’ve, uh... misplaced... my knife.
101) MAXKID
[young]{normal} You probably left it in the real Stinky’s body.{happy} It’s an honest mistake.
102) STINKY
{pretentious}[forced good-natured] Oh, that’s silly, you little rascals!
103) STINKY
{normal}[menacing] Now run along and play before I call child services.
104) 
A complete balanced breakfast.
105) SAMKID
[young] A bowl of Oyster Smacks, half an orange, a poached egg, and beet juice.
106) MAXKID
[young] And I want a bowl of Algae-Bits, because they make the milk turn green!
107) STINKY
Yeah, I don’t have any of that.
108) SAMKID
[young] A double martini, with an absinthe chaser.
109) MAXKID
{surprised}[young] Served on a stack of porno mags!
110) STINKY
This is a set-up, right?
111) SAMKID
[young] Pretty much everything we say generally is, yes.
112) 
A double martini.
113) SAMKID
{angry}[young] Nothing from this dumb old place.
114) MAXKID
{angry}[young] Yeah!
115) STINKY
Suit yourselves.
117) SAM
Hiya, Stinky!
