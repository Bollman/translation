1) SAM
Why did we come here?
2) MAX
To drink ourselves into not caring that the volcano was about erupt and kill us all?
3) SAM
Well, that was a bad idea.
4) MAX
It’d be cheaper to do something about the eruption.
5) SAM
Can’t talk now, the volcano could erupt at any moment!
6) MAX
Fine. Choose a volcano over your best friend.
7) SAM
Hey, Max.
8) MAX
Yo.
9) SAM
This place is pretty nice, isn’t it, Max?
10) MAX
I’m nervous, Sam!
11) MAX
What if the paparazzi get a picture of me hanging out in a... [sotto voce] baby bar?
12) MAX
Think of the rumors!
13) 
Babies know how to party.
14) SAM
I still say this is a nice bar.
15) MAX
Eh. The Tiki Crib across town has a ball pit AND a changing table.
16) 
Best. Baby Bar. Ever.
17) SAM
Aren’t people here phony?
18) MAX
I don’t know, that Earhart chick’s got it going on. I’m gonna try to get her number.
19) MAX
Will you be my wingman?
20) SAM
Max, that’s wrong on more levels than I can count.
21) SAM
See anything that will help us stop that volcano, Max?
22) MAX
Well, we could make a human sacrifice.
23) SAM
These guys don’t deserve that kind of treatment!
24) MAX
Okay, fine. Canine sacrifice.
25) 
Shame this’ll all be destroyed by lava.
26) SAM
It’s nice getting some R&R away from the office.
27) MAX
As long as you mean “Running away from the deadly volcano and Renal failure as you’re burned by lava and suffocated by ash.”
28) 
Let’s just relax.
29) 
So long.
30) SAM
Talk to you later.
31) MAX
Call me!
