1) MAX
Hey, Sam,{realizing} look! Clues!
2) SAM
{suspicious}Hmmm. My wilderness tracking skills have gotten rusty, but something tells me this just might be the ancient entrance to the secret underground lair of the volcano god.
3) HOFFA
{angry}Where do you two think you’re going?
4) MAX
[delighted]{happy} Oh, Sam, he’s got a little gun isn’t he the cutest thing? Can we keep him?
5) SAM
{normal}Try not to get imprinted or shot, Max. We still don’t know who this little tyke belongs to.
6) HOFFA
Jimmy Hoffa belongs to nobody!
7) 
We beat the bucking surfboard!
8) 
How do we get in the Waitresses’ Union again?
9) SAM
{confused}How do we get in the Waitresses’ Union again?
10) HOFFA
Prove you can keep from spilling on the paying customers. Take a tray of drinks and ride the bucking surfboard without dropping anything.
11) SAM
You can’t be Jimmy Hoffa!
18) 
You can’t be Jimmy Hoffa!
12) SAM
{incredulous}Jimmy Hoffa was a ruthless union organizer and head of the Teamsters in the 50s and 60s, known for his ties to organized crime and his mysterious disappearance!
13) MAX
Good recap, Sam. Also,{suspicious} Jimmy Hoffa wasn’t a diaper-wearing baby.
14) HOFFA
{angry}I’m not a baby!
15) MAX
{realizing}Look at the way his little thumb pulls back the hammer!{happy} He’s adorable!
16) HOFFA
{sad}People always showing me disrespect.
17) HOFFA
{angry}I should never have started drinking from that fountain of youth.
19) SAM
{confused}What fountain of youth are you talking about?
20) HOFFA
{angry}That fountain right there, brainiac!
21) SAM
{sarcastic}You expect us to believe THE fountain of youth is on Easter Island?
22) MAX
{realizing}I can’t believe it!{happy} I can’t believe how perfect he is!
23) 
What fountain of youth?
24) SAM
{incredulous}You’ve been hitting the fountain water a little hard, haven’t you?
25) HOFFA
I do partake on occasion, yes.{angry} That’s MY business.
26) MAX
{realizing}He likes his fountain of youth water, doesn’t he?{happy} Yes he does! Yes he does!
27) HOFFA
{normal}Little bit. Little bit.
28) 
You came for the waters?
29) SAM
{confused}Why’d you drink yourself back to infancy?
30) HOFFA
{angry}Look I ain’t got a problem so don’t go sayin’ that I do.
31) HOFFA
{normal}It’s just a little discretion is required.
32) HOFFA
Drink too little,{sad} and you have to go through puberty again. Too much,{angry} and bada bing.
33) 
Why’d you drink so much?
34) SAM
Step aside, sport. We grown-ups have to {stern}take care of a great big mean old volcano god.
35) HOFFA
You and your companion{angry} will be wanting to take a step back.
36) MAX
[delighted]{realizing} Oh look he’s trying to intimidate us!{happy} He’s just perfect! Please let’s take him home!
37) SAM
Not now, Max.
38) MAX
{sad}Then when, Sam?{normal} My biological clock is ticking, {stern}and I’m not getting any younger.
39) 
Move it, short stuff.
40) SAM
Play time is over, son. We’ve got important work to do.
41) HOFFA
I’m expanding my enterprises back here, {angry}and I’d HATE{normal} for either of you to {angry}get HURT{normal} in the construction.
42) SAM
We’ll take that risk. We’re freelance police.
43) HOFFA
Freelancers?!? {angry}That’s worse than scabs!
44) HOFFA
[slow burn baby wail] {sad}WAAAAAAAAAAAAAHHHHHH!
45) 
Seriously, let us in.
48) 
Seriously, let us in.
46) SAM
Seriously, champ. We have to get into that cave.
47) HOFFA
{angry}Beat it!
49) SAM
Is baby getting fussy? Is it his bedtime?
50) HOFFA
{angry}Stop it!
51) MAX
{realizing}I wonder if his precious little skull is soft and malleable like a real baby’s!
52) MAX
{confused}How can we find out?
53) 
Is baby sleepy?
54) SAM
Is baby gassy? Does baby need to be burped?
55) HOFFA
{angry}Get outta here!
56) 
Is baby gassy?
57) SAM
We’ll give you a nice big rattle for that dumb old gun of yours!
58) HOFFA
{angry}You two are seriously trying my patience.
59) MAX
Sam, let’s teach him the wrong words for things!
60) 
Give us that gun.
61) 
So long.
62) SAM
We’re going bye bye now. {happy}Bye bye!
63) HOFFA
{angry}You’ll stay gone if you know what’s good for you!
64) MAX
Sam, look at how his little eyes and the barrel of his gun follow us!{happy} I could squeal!
65) SAM
Bye bye now!
