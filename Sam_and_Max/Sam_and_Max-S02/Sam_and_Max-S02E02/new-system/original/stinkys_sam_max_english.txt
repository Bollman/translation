1) SAMKID
[young] Stinky’s looks so much bigger than I remember it.
2) MAXKID
[young] It was always big. It’s the health inspectors that got small.
3) SAMKID
[young] Do you have any money?
4) MAXKID
[young] Yeah, but ever since I shrunk I can’t get to it anymore.
5) SAMKID
[young] What? Where do you keep it... oh, right.
6) SAM
Hey, Flint’s gone!
7) MAX
Darn!  There’s a jar I wanted opened.
8) SAM
[a little offended]{confused} Why not ask me?  [emphasis on “me”]
9) MAX
{confused}With your buttery soft hands?{sarcastic}  I don’t think so.
10) 
Where’s Flint?
11) SAM
That volcano threatening to obliterate Easter Island sure seems insignificant now that it’s half a world away.
12) MAX
I think you just encapsulated my whole foreign policy in a single sentence!
13) 
Forget Easter Island!
14) SAM
I don’t know how to tell you this, Max, but... I think this new so-called “Stinky” impostor murdered the real Stinky!
15) MAX
Well, duh!
16) SAM
We’ll have to keep an eye on her.
17) MAX
{confused}Want me to break your legs so you can convalesce in an apartment across the street, while I occasionally flit in wearing diaphanous gowns and discuss the case as a metaphor for our relationship?
18) SAM
Hmmm... I’m trying to think of a much stronger word for “no.”
19) 
I think Stinky killed Stinky!
20) SAM
How do you think Stinky got rid of the body?
21) MAX
The pie does look a little whiskery today...
22) 
Where’s the body?
23) SAM
You’d think the Board of Health would do something about this place.
24) MAX
Or PETA.  Or Greenpeace.  Amnesty International, the Red Cross, the W.H.O., the A.C.L.U., the C.D.C., or the Vatican.
25) SAM
Greenpeace was here last week, but they had trouble dragging the boats along the sidewalk.
26) 
How is this place still open?
27) SAM
Have you ever tried Stinky’s coffee?
28) MAX
I use it to degrease the pistons in my Wii.
29) SAM
That sounds more disgusting than it really is.
30) MAX
Like Shia LaBeouf!
31) 
Have you tried the coffee?
32) SAM
Feel like stopping for a bite to eat, Max?
33) MAX
{happy}Sure!  {normal}Sybil’s a fast runner, she’ll be all right out there for a while.
34) SAM
It’s fascinating how other people’s problems fade away the minute a door shuts between you.
35) 
Hungry?
36) 
Let’s go.
37) SAM
I’m out of things to say.
38) MAX
And I’m out of minty-flavored toothpicks!  Excuse me while I look for some.
