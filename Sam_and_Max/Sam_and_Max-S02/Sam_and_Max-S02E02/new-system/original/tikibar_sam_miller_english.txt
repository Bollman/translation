1) MILLER
{happy}Hiya, boys. {normal}You’re new here, right?
2) SAM
Sam & Max, Freelance Volcano Stoppers.
3) MILLER
Well gosh, I’m glad to meet ya. I’m Glenn Miller.
4) MILLER
Maybe you boys can help me out.
5) SAM
{confused}How’s progress going on your song?
6) MILLER
Oh, that. I released it already.
7) MAX
{surprised}That was fast!
8) MILLER
{happy}Digital distribution, boys.{sad} The hard part is fitting DRM into the conch shells.
9) MILLER
{happy}Hiya, fellas. {normal}I was wondering. You boys going to do anything about{sad} that volcano?
10) SAM
We’re working on it.
11) MILLER
{happy}Swell.
12) SAM
{confused}You’re a missing person?
13) MAX
We thought you were dead!
14) MILLER
I get that a lot. I was here twenty years before *I* believed I wasn’t really dead.
15) EARHART
Hated to break it to you, Glenn, but {angry}all that harp playing was getting annoying.
16) 
You were missing?
17) SAM
How’d you end up here?
18) MILLER
I was on a mission for the Air Force and I flew here.
19) SAM
{worried}You mean your plane was sucked through a portal into an alternate dimension and deposited here.
20) MILLER
No, I just got kind of lost and crashed on Easter Island.
21) MILLER
You ever get a song stuck in your head and can’t get it out?{angry} It’s downright distracting!
22) SAM
{confused}Why didn’t you ever go back home?
23) MILLER
I can’t go back now without a big comeback hit! It’d be anti-climactic.
24) 
Why haven’t you left?
25) SAM
{confused}What do you need help with?
30) 
What do you need help with?
26) MILLER
I’m trying to find just the perfect sound for my{happy} next big hit.
27) MAX
{happy}Oh, is that all?
28) MILLER
{happy}Well gee, thanks, {normal}but I don’t want to just put out a repeat of my early stuff. It’s gotta be new and fresh.
29) MILLER
{angry}But I’m stuck on two things: {normal}I need a catchy melody, and I need a good train sound.
31) SAM
[incredulous] {incredulous} You need a melody and a train whistle for your song?
32) MILLER
You bet! I’m pullin’ out all the stops!{happy} This song’s gonna be about all the things the kids are into these days.
33) MILLER
Trains! And... and sock hops! Swing dancing! {sad}Methamphetamines!
34) MAX
{confused}But how are we going to find a new melody?{normal} You’ve been here 60 years and haven’t been able to!
35) MILLER
{normal}Gimme a break, guys.{sad} I’m teething.
36) MILLER
{normal}Baby Lindbergh Baby tells me that... that{happy} “electronic” music is popular now. Maybe I should try that.
37) 
You need a melody and a train whistle?
38) MILLER
You bet!
39) SAM
[imitating train whistle]{realizing} Woooo Woooo!
40) MILLER
Nice try, fellas,{sad} but it’s a little pitchy.{normal} Get me a good melody and a convincing train sound,{happy} and we’ll be all set!
41) 
A melody and a train whistle, huh?
42) SAM
How’s the song coming?
45) 
How’s the song coming?
43) MILLER
{happy}Well, the train sound you got me is pretty good, {sad}but I’m still stuck on the melody.
46) MILLER
{happy}Well, that melody is really catchy all right, {normal}but I’m still missing that perfect train sound.
47) SAM
Was your song a big hit?
48) MILLER
Well, {sad}SPIN said it was “hopelessly dated” and Blender called it{angry} “Chattanooga Doo Doo.”
49) MAX
{happy}Tee hee!
50) SAM
{confused}So you decided to update your sound?
51) MILLER
{sad}No, I decided to say{angry} SCREW those guys! What they know about music isn’t fit to fill my diaper!
52) 
How’d the train song do?
53) SAM
What are you working on now?
54) MILLER
I’m trying some more experimental things. Spoken word pieces, hip hop, some found percussion.
55) MAX
{stern}And no more of that crusty old “songs about trains” nonsense.
56) MILLER
Gosh, no!{happy} My new piece is going to be called “Meet Me on the Mag-Lev.”
57) 
What are you up to now?
58) SAM
{confused}What’s Benny Goodman like in person?
59) MILLER
{happy}Benny could do things with a clarinet that nobody else could.
60) MILLER
Thank god.{normal} One night after a show in New York... well, I’ll just tell you it was the damndest thing I ever saw, and leave it at that. There’s a lady present.
61) 
What’s Benny Goodman like?
62) SAM
{confused}Was Count Basie really a vampire?
63) MILLER
A golem, actually, but we never held that against him.
64) MAX
{realizing}Those were simpler times.
65) 
What’s Count Basie like?
66) 
So long.
67) SAM
{happy}Keep on rockin’, Glenn.
68) MILLER
{happy}Word.
69) 
We’ve got a train sound for you.
70) MAX
[imitating electronic back beat]{aloof} Boom boom chick chick boom!
